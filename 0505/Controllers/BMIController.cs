﻿using _0505.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace _0505.Controllers
{
    public class BMIController : Controller
    {
        public ActionResult Index()
        {
            return View(new BMIData());
        }
           // GET: BMI
           [HttpPost]
        public ActionResult Index(BMIData data)
        {
            //bool a = true;
            //if (data.height < 50 || data.height > 200)
            //{
            //    ViewBag.heightError = "身高請輸入50~200的數值";
            //    a = false;
            //}
            //if (data.weight < 50 || data.weight > 200)
            //{
            //    ViewBag.weightError = "體重請輸入30~150的數值";
            //    a = false;
            //}
            if (ModelState.IsValid)
            {

                var m_height = data.height / 100;
                var result = data.weight / (m_height * m_height);
                ViewBag.h = data.height;
                ViewBag.w = data.weight;
                var level = "";
                if (result < 18.5)
                {
                    level = "體重過輕";
                }else if(result>=18.5&& result < 24)
                {
                    level = "正常範圍";
                }else if(result >= 24 && result < 27)
                {
                    level = "過重";
                }else if(result >= 27 && result < 30)
                {
                    level = "輕度肥胖";
                }else if(result >= 30 && result < 35)
                {
                    level = "中度肥胖";
                }else if (result >= 35)
                {
                    level = "重度肥胖";
                }
                data.result = result;
                data.level = level;
            }
            return View(data);
        }
    }
}